/*
Copyright © 2022 Lindsay Steele <lgsteele@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package certutils

import (
	"crypto/tls"
	"encoding/json"
	"errors"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"strconv"
)

type Site struct {
	ID           int
	Name         string // Display Name for Monitoring System
	Hostname     string // Host name expected in a certificate
	IP           string // (optional) IP Address expected if any is used.
	CheckAddress string // The address that will be used for the check,  can be an IP or a hostname.
	CheckPort    int
	SkipVerify   bool // True or False - Whether to skip certificate Verification - set to true for Self Signed.
}

// GetSiteList Method to get and then return a slice of Sites loaded in from the Configuration
//  Used internally to Find Sites and Externally when constructing the Discovery.
func GetSiteList() []Site {
	var Sites []Site
	viper.UnmarshalKey("Sites", &Sites)
	return Sites
}

func GetSiteForID(id int) (Site, error) {

	// Initialise a site
	var site Site

	// Get the list of sites .... should we maybe capture this error, do testing and find out.
	siteList := GetSiteList()

	// loop over the struct looking for the ID.
	for _, v := range siteList {
		if v.ID == id {
			site = v
			// Found a site so return it with a nil error
			return site, nil
		}
	}
	// We didn't find anything,  so return the empty site with an error
	return site, errors.New("site: Site with the ID Given was not found")
}

// Method to get A Site for an ID attached to from the SiteList

//func GetSite([]Site) Site {
//
//
//	return foundSite
//}

func GetConfig(skipVerify bool) tls.Config {
	config := tls.Config{
		InsecureSkipVerify: skipVerify,
		//ServerName: "ingress.local",
	}
	return config
}

func GetConfigValidity() tls.Config {
	config := tls.Config{
		//InsecureSkipVerify: skipVerify,
		//ServerName: "ingress.local",
	}
	return config
}

// GetConnection If there is an error here we log and bug out.
func GetConnection(config tls.Config, address string, port int) *tls.Conn {
	fullAddress := address + ":" + strconv.Itoa(port)
	log.Debug("Probing Address: " + fullAddress)
	conn, err := tls.Dial("tcp", fullAddress, &config)
	if err != nil {
		log.Fatal("Server doesn't support SSL certificate err: " + err.Error())
	}
	return conn
}

// GetConnectionResilient GetConnection If there is an error here we log and bug out.
func GetConnectionResilient(config tls.Config, address string, port int) (*tls.Conn, string, string) {
	fullAddress := address + ":" + strconv.Itoa(port)
	log.Debug("Probing Address: " + fullAddress)
	conn, err := tls.Dial("tcp", fullAddress, &config)
	validity := "Valid"
	validityMessage := ""
	if err != nil {
		//log.Fatal("Server doesn't support SSL certificate err: " + err.Error())
		validity = "Invalid"
		validityMessage = err.Error()
		newConfig := GetConfig(true)
		conn, _ = tls.Dial("tcp", fullAddress, &newConfig)
		//return conn, "Invalid", err.Error()
	}
	return conn, validity, validityMessage
}

// GetValidity GetConnection If there is an error here we log and bug out.
func GetValidity(config tls.Config, address string, port int) (string, string) {
	fullAddress := address + ":" + strconv.Itoa(port)
	log.Debug("Probing Address: " + fullAddress)
	_, err := tls.Dial("tcp", fullAddress, &config)
	if err != nil {
		log.Debug("Found Validity Error: " + err.Error())
		return "Invalid", err.Error()
	}
	return "Valid", ""
}

// GetDiscovery Get the discovery JSON from Settings.
func GetDiscovery() string {

	siteList := GetSiteList()

	jsonOutput, _ := json.MarshalIndent(siteList, "", " ")

	//fmt.Println(string(jsonOutput))

	return string(jsonOutput)
}
