### v0.3.1 - Minor Fixups 2022-09-24)

* Templates now only do checks everyone one hour instead of 1 minute that was used during testing
* Triggers for no data have been moved to 3 hours instead of the 2 minutes used during testing
* Template expiry days have been moved from 30 to 27 days taking into consideration that some sites auto renew certificates at 28 days.
* The executable now has the default permissions used by ZabbixSrv.   This might still have to be changed in some cases but may work by default in others.
* Updates to documentation to reflect when the discovery might be useful.

### v0.3.0 - Host Template Functionality (2022-09-20)

* Documentation now better details the dual functionality of the application 
* There is now a new template to support the application being used as an external script to monitor existing hosts rather than using configuration file lookup.
* The triggers on the templates now include dependencies so only the most recent value is shown.
* Info command functionality is now working for manual diagnostics.
* The Template items for expiry days have now been changed to Floating point values
  * This allows negative numbers which Zabbix will not handle with unsigned integer functionaity.
* The Template Expiry days Warning has been changed from 30 to 26 days due the fact that some certificates will auto-renew at 28 days.
* Tags have been added to all checks for Application: Certificates to allow easy viewing of latest data.
* Removed the ExpiryDate command as it is not really used and date functionality can be seen in the info command
  * Can easily add this command in future if needed.

### v0.2.1 - Initial Release (2022-08-29)

* Zabbix Templates are now included
  * Additonal items to check for errors in log file
* Certificate verification (Validity Checking) is now funcitonal with an included Zabbix Template

### v0.1.1 - Initial Release (2022-09-04)

* Basic functionality including:
  * ExpiryDays
  * ZabbixDiscovery
  * Settings File (Viper)
  * Command Line parameters functionality (Cobra)
  * Logging Functionality