checkcert - Certificate Expiry and Information Monitoring for Zabbix
===========================

# Overview
checkcert is a small utility designed to assist in certificate expiry monitoring for Linux and Zabbix.  This utility has been tested with Zabbix 6.0

checkcert includes two basic modes of operation when used with Zabbix:

**External Script Mode:** This is where the application works as an external script within Zabbix.  This means that checks 
are done via templates assigned to the host and checks are run from the Zabbix server or a Proxy directly.  This will be most
useful when monitoring hosts that being monitored from the Zabbix Server or Proxy.

**Zabbix Agent Mode:** This is where the application works as a UserParameter on the Zabbix Server or one of the external hosts monitoring a number of hosts
configured via a configuration file.  This mode is most useful when a large range of sites need to be monitored from a remote monitored server
endpoint or when you ae wanting to monitor a host that is configured via domain names when the host is primarily accessible via an IP Address.

In both modes, additional checks at the template level monitor the configuration file for errors or the remote hosts for lack of data coming back to Zabbix.

There is also the ability to create custom checks passing parameters should that be required.

# Pre requisites

This application has only been tested on RHEL 8 with Zabbix Server and Agent 6.0. 

The templates have been created to work with Zabbix Server 6.0 may work on later Zabbix versions.

Stand alone tools should work on any x86_64 Linux distribution.

# Installation and Setup

Installation is done manually completing the following tasks.

## Setup ExternalScripts

### Requirements

* Zabbix Server or Proxy 6.0+ that has access to the hosts that need monitoring for certificate expiry. 
* Linux Distribution

### Installation

* These tasks will need to be done on a server with a Zabbix Agent and visibility of the servers with certificates that need to be monitored.
* Copy the Installation Archive to Zabbix Server and extract it.
```
tar -xvzf checkcert-[version].tar.gz
```
* Copy the "checkcert" executable to the ExternalScripts directory.  This will most likely be "/var/lib/zabbixsrv/externalscripts/"
```
cp checkcert /var/lib/zabbixsrv/externalscripts/
```
* Copy the configuration file to the Zabbix Server home directory
```
cp checkcert.yml /var/lib/zabbixsrv/
```
* Confirm directory ownership to the zabbixsrv account.
```
chown -R zabbixsrv:zabbixsrv /var/lib/zabbixsrv/
restorecon -FRvv /var/lib/zabbixsrv/
```
* Create the logrotation directory and install the logrotation definition
```
cp checkcert-rotate /etc/logrotate.d/checkcert
mkdir /var/log/checkcert
chown zabbixsrv /var/log/checkcert
``` 
* Review the settings configuration in /etc/checkcert/checkcert.yml and adjust as required.   At this stage, logging is all that is most likely required but later the sites to be monitored will need to be added.
 
* On the Zabbix Server, import the certificate named "ZabbixCertificateExpiryViaAgentDiscoveryTemplate.yaml" from the Templates directory into Zabbix.

### Usage

* Create or select a host in Zabbix that has the certificate to be monitored and add the Template to the host.
* Adjust Macros as required.
* If issues are encountered, watch for and add any required SE Linux rules.

## Setup Linux Agent Discovery Mode

### Requirements

* Zabbix Agent with visibility of the hosts that need to be monitored for certificate expiry and validation.

### Installation

* These tasks will need to be done on a server with a Zabbix Agent and visibility of the servers with certificates that need to be monitored.
* 
* Copy the archive to the Zabbix server and extract it:
```
tar -xvzf checkcert-[version].tar.gz
```
* Copy the "checkcert" executable to the /usr/local/bin directory and adjust permissions. 
```
cp checkcert /usr/local/bin/
chown -R zabbix:zabbix /usr/local/bin/checkcert
restorecon -FRvv /usr/local/bin
```
* Copy the configuration file to the Zabbix Server home directory
```
mkdir /etc/checkcert
cp checkcert.yml /etc/checkcert/
```
1``
* Create the logrotation directory and install the logrotation definition
```
cp checkcert-rotate /etc/logrotate.d/checkcert
mkdir /var/log/checkcert
chown zabbixsrv /var/log/checkcert
``` 
* Add the following lines to the Zabbix Agent configuration file (zabbix_agentd.conf) and restart the agent when completed:
```
UserParameter=checkcert.discovery,checkcert zabbixdiscovery
UserParameter=checkcert.expirydays[*],checkcert expirydays --id=$1
```
* Review the settings configuration in /etc/checkcert/checkcert.yml and adjust as required.   At this stage, logging is all that is most likely required. 
* Import the certificate named "ZabbixCertificateExpiryViaExternalScriptsTemplate.yaml" from the Templates directory into Zabbix.

### Usage

* The imported certificate should be assigned to the host doing the discovery whether this is remote or the Zabbix Server.

When using the application in Agent mode, configuration of the application level is done via /etc/checkcert/checkcert.yaml where the sites to be configured are defined in the configuration file.   These sites are then discovered by the application and added as checks that originate from the host with the agent.

Samples of the host definitions are seen in the configuration file in yaml format.






