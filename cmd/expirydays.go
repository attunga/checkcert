/*
Copyright © 2022 Lindsay Steele <lgsteele@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/attunga/checkcert/certutils"
	"os"
	"time"
)

var expirydaysCmd = &cobra.Command{
	Use:   "expirydays",
	Short: "Returns the number of days until a certificate expires",
	Long: `Returns an Integer value of the number of days until a certificate expires
Always use parameter= when passing parameters.
`,
	//Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {

		// The ID of the domain will take preference if it is given so we see if it was passed.
		if cmd.Flags().Lookup("id").Changed {

			log.Debug("ID was passed with value: ", cmd.Flags().Lookup("id").Value)

			// Get the site object for the ID passed
			id, _ := cmd.Flags().GetInt("id")

			siteForID, err := certutils.GetSiteForID(id)

			if err != nil {
				log.Fatal("Could not find ID: ", err.Error())
			}

			// Get the tls config using a value from the siteForID
			config := certutils.GetConfig(siteForID.SkipVerify)

			// Get the connection object... maybe I need error handling here
			conn := certutils.GetConnection(config, siteForID.CheckAddress, siteForID.CheckPort)

			// Get the certificate expiry
			expiry := conn.ConnectionState().PeerCertificates[0].NotAfter

			// Calculate Days to expiry first in Unix time and then in Days
			expiryDays := (expiry.Unix() - time.Now().Unix()) / 86400

			log.Debug("Calculated days to Certificate Expiry: ", expiryDays)

			//fmt.Printf("Issuer: %s\nExpiry: %v\n", conn.ConnectionState().PeerCertificates[0].Issuer, expiry.Format(time.RFC850))
			fmt.Println(expiryDays)
			return
		} else if cmd.Flags().Lookup("address").Changed {
			// If the address is passed we are then in manual mode,  with the address all we really need, everything else is defaults.
			log.Debug("skip-validation seen as: ", cmd.Flags().Lookup("skip-validation").Value)
			log.Debug("Address was passed with value: ", cmd.Flags().Lookup("address").Value)

			// Get the parameter passed on whether we skip validation or not - the default is to skip validation.
			skipValidation, _ := cmd.Flags().GetBool("skip-validation")

			// Get the tls config
			config := certutils.GetConfig(skipValidation)

			// Extract out the address and Port - We are going to ignore errors and let the connection fail should that
			// be needed.
			address, _ := cmd.Flags().GetString("address")
			port, _ := cmd.Flags().GetInt("port")

			// Get the connection object.
			conn := certutils.GetConnection(config, address, port)

			// Get the certificate expiry
			expiry := conn.ConnectionState().PeerCertificates[0].NotAfter

			// Calculate Days to expiry first in Unix time and then in Days
			expiryDays := (expiry.Unix() - time.Now().Unix()) / 86400

			log.Debug("Calculated days to Certificate Expiry: ", expiryDays)

			//fmt.Printf("Issuer: %s\nExpiry: %v\n", conn.ConnectionState().PeerCertificates[0].Issuer, expiry.Format(time.RFC850))
			fmt.Println(expiryDays)
			return
		} else {
			// Bugging out if not valid options are given
			log.Error("No valid parameter was passed")
			cmd.Help()
			os.Exit(0)
		}

	},
}

func init() {
	rootCmd.AddCommand(expirydaysCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// expirydaysCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// expirydaysCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	//expirydaysCmd.Flags().BoolP("manual", "m", false, "Manual Mode - requires address")
	expirydaysCmd.Flags().StringP("address", "a", "", "Address or manual mode address as argument")
	expirydaysCmd.Flags().IntP("port", "p", 443, "Port to use in manual mode")
	expirydaysCmd.Flags().IntP("id", "i", 1, "ID of the domain to check from the configuration")
	expirydaysCmd.Flags().BoolP("skip-validation", "s", true, "Optional - Skip certificate validation")

	//expirydaysCmd.MarkFlagsRequiredTogether("manual", "address")
	//wxpirydaysCmd.MarkFlagsMutuallyExclusive("id", "manual")
	expirydaysCmd.MarkFlagsMutuallyExclusive("id", "address")
	expirydaysCmd.MarkFlagsMutuallyExclusive("id", "port")

	//expirydaysCmd.Flags().String("domain", "a", "", "Help message for toggle")
	//expirydaysCmd.
}

// Function to Calculate Days until expiry.
