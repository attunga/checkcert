/*
Copyright © 2022 Lindsay Steele <lgsteele@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/attunga/checkcert/certutils"
	"os"
)

// validityCmd represents the validity command
var validityCmd = &cobra.Command{
	Use:   "validity",
	Short: "Check a Certificate Validity",
	Long: `Checks a Certificate Validity using either manual means or from the sites list
in the configuration file.

If using the Configuration file options,  there is the ability to skip validation which can be 
useful when doing templates with auto discovery.`,
	Run: func(cmd *cobra.Command, args []string) {
		log.Debug("validity called")

		// The ID of the domain will take preference if it is given so we see if it was passed.
		if cmd.Flags().Lookup("id").Changed {

			log.Debug("ID was passed with value: ", cmd.Flags().Lookup("id").Value)

			// Get the site object for the ID passed
			id, _ := cmd.Flags().GetInt("id")

			siteForID, err := certutils.GetSiteForID(id)

			if err != nil {
				log.Fatal("Could not find ID: ", err.Error())
			}

			if siteForID.SkipVerify {
				log.Debug("Found Skipped so bugging out")
				fmt.Println("Skipped")
				os.Exit(0)
			}

			// Get the generic tls config
			config := certutils.GetConfigValidity()

			// Get the connection object... maybe I need error handling here
			validityStatus, validityVerboseMsg := certutils.GetValidity(config, siteForID.CheckAddress, siteForID.CheckPort)

			verbose, _ := cmd.Flags().GetBool("verbose")

			if verbose {
				fmt.Println(validityStatus, validityVerboseMsg)
			} else {
				fmt.Println(validityStatus)
			}

		} else if cmd.Flags().Lookup("address").Changed {
			// If the address is passed we are then in manual mode,  with the address all we really need, everything else is defaults.
			log.Debug("Address was passed with value: ", cmd.Flags().Lookup("address").Value)

			// Get the generic tls config
			config := certutils.GetConfigValidity()

			// Extract out the address and Port - We are going to ignore errors and let the connection fail should that
			// be needed.
			address, _ := cmd.Flags().GetString("address")
			port, _ := cmd.Flags().GetInt("port")

			// Get the connection object.
			//conn := certutils.GetConnection(config, address, port)

			// Get the connection object... maybe I need error handling here
			validityStatus, validityVerboseMsg := certutils.GetValidity(config, address, port)

			verbose, _ := cmd.Flags().GetBool("verbose")

			if verbose {
				fmt.Println(validityStatus, validityVerboseMsg)
			} else {
				fmt.Println(validityStatus)
			}
		} else {
			// Bugging out if not valid options are given
			log.Error("No valid parameter was passed")
			cmd.Help()
			os.Exit(0)
		}

	},
}

func init() {
	rootCmd.AddCommand(validityCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// validityCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// validityCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	validityCmd.Flags().StringP("address", "a", "", "Address or manual mode address as argument")
	validityCmd.Flags().IntP("port", "p", 443, "Port to use in manual mode")
	validityCmd.Flags().IntP("id", "i", 1, "ID of the domain to check from the configuration")
	validityCmd.Flags().BoolP("verbose", "v", false, "Optional - Skip certificate validation")

	validityCmd.MarkFlagsMutuallyExclusive("id", "address")
	validityCmd.MarkFlagsMutuallyExclusive("id", "port")

}
