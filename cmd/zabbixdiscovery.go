/*
Copyright © 2022 Lindsay Steele <lgsteele@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/attunga/checkcert/certutils"

	"github.com/spf13/cobra"
)

// zabbixdiscoveryCmd represents the zabbixdiscovery command
var zabbixdiscoveryCmd = &cobra.Command{
	Use:   "zabbixdiscovery",
	Short: "Displays the JSON required by Zabbix Discovery.",
	Long: `Displays the JSON required by Zabbix Discovery.
`,
	Run: func(cmd *cobra.Command, args []string) {
		log.Debug("Zabbix Discovery was called")

		// Get the text from CertUtils and display it to screen
		fmt.Println(certutils.GetDiscovery())
	},
}

func init() {
	rootCmd.AddCommand(zabbixdiscoveryCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// zabbixdiscoveryCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// zabbixdiscoveryCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
