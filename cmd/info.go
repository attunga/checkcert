/*
Copyright © 2022 Lindsay Steele <lgsteele@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/attunga/checkcert/certutils"
	"time"
)

// infoCmd represents the info command
var infoCmd = &cobra.Command{
	Use:   "info",
	Short: "A full list of certificate information in JSON or human readable formats.",
	Long: `A full list of certificate information in either JSON or
human readable format.

This includes a range of parameters about a certificate in either JSON format
for use by monitoring programs or in human readable format by passing -h for 
debugging purposes.`,
	Run: func(cmd *cobra.Command, args []string) {
		log.Debug("info called")
		// If the address is passed we are then in manual mode,  with the address all we really need, everything else is defaults.
		log.Debug("Address was passed with value: ", cmd.Flags().Lookup("address").Value)

		// Get the tls config
		config := certutils.GetConfig(false)

		// Extract out the address and Port - We are going to ignore errors and let the connection fail should that
		// be needed.
		address, _ := cmd.Flags().GetString("address")
		port, _ := cmd.Flags().GetInt("port")

		// Get the connection object.
		conn, _, _ := certutils.GetConnectionResilient(config, address, port)

		// Get the certificate expiry
		expiry := conn.ConnectionState().PeerCertificates[0].NotAfter

		// Calculate Days to expiry first in Unix time and then in Days
		expiryDays := (expiry.Unix() - time.Now().Unix()) / 86400

		// Get the connection object... maybe I need error handling here
		validityStatus, validityVerboseMsg := certutils.GetValidity(config, address, port)

		fmt.Printf("Site Address: %s:%v\n", address, port)
		fmt.Printf("Certificate Expiry: %s\n", expiry.Format(time.RFC850))
		fmt.Printf("Days Until ExpiryL: %v\n", expiryDays)
		fmt.Printf("Issuer: %s\n", conn.ConnectionState().PeerCertificates[0].Issuer)
		fmt.Printf("Validity Status: %s\n", validityStatus)
		fmt.Printf("Validity Message: %s\n", validityVerboseMsg)

	},
}

func init() {
	rootCmd.AddCommand(infoCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// infoCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// infoCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	infoCmd.Flags().StringP("address", "a", "", "Address or manual mode address as argument")
	infoCmd.Flags().IntP("port", "p", 443, "Port to use in manual mode")
}
