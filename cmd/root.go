/*
Copyright © 2022 Lindsay Steele <lgsteele@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/attunga/checkcert/utils"
	"os"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "checkcert",
	Short: "An application to check the expiry times and validity of certificates",
	Long: `An application to check the expiry times and validity of certificates.

This application may be useful for a range of monitoring applications but has discovery options to work 
in with Zabbix`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.checkcert.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".checkcert" (without extension).
		viper.AddConfigPath(home)
		viper.AddConfigPath("/etc/checkcert/") // path to look for the config file in
		viper.AddConfigPath("/etc/zabbix/")
		viper.AddConfigPath(".") // optionally look for config in the working directory
		viper.SetConfigType("yaml")
		viper.SetConfigName("checkcert.yaml")
	}

	// Set the custom formatter to give the date in a required format as part of the logs.
	// Setting this early in case a timestamp is needed on a fatal call.
	utils.SetupLogFormatting()

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		log.Debug(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
	// This is to capture errors reading in the configuration file that are not seen in the command above
	err2 := viper.ReadInConfig()
	if err2 != nil {
		log.Fatal("Error reading configuration file: ", err2.Error())
	}

	// Check we have a value for the config file,  if not then we exit with a message.
	if viper.ConfigFileUsed() == "" {
		log.Error("Could not find the config file,  please ensure a config file named check.toml is in the /etc/stroomc or the program directory")
		os.Exit(1)
	}

	// Move to a debug directive
	log.Debug("Config File:", viper.ConfigFileUsed())

	// Set the log levels here .. default to nothing if the config is not sane.
	// Seems to work best here as it is the point we have initialised the config file
	utils.SetUpLogging()

	viper.AutomaticEnv() // read in environment variables that match
}
